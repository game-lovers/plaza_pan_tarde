﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using OVR;

public class RayController : MonoBehaviour
{
    //Linea en pantalla
    LineRenderer line;

    //Panel con el texto
    //public GameObject panel;

    //Personaje a teletransportar
    public Transform player;

    //Boton necesario para el teleport. 
    public OVRInput.Button buttonTeleport;
    public OVRInput.Button buttonDoor;
    public GameObject door;

    // Esto se ejecuta una vez al pulsar el Play
    void Start()
    {
        //Tomamos la referencia del componente Line Renderer del objeto actual
        line = GetComponent<LineRenderer>();
    }

    // Esto se ejecuta todo el tiempo en bucle mientras estamos en play
    void Update()
    {
        //Definimos la posición inicial de la línea visual
        line.SetPosition(0, transform.position);

        //Creamos una variable para guardar la información del punto de impacto
        RaycastHit hit;

        //Lanzamos el Raycast: un rayo imaginario desde la mano hacia el vector Rigth, 
        //que en este caso es la dirección de los dedos
        if (Physics.Raycast(transform.position, transform.right, out hit))
        {
            //Ponemos como posición final de la línea el punto de impacto
            line.SetPosition(1, hit.point);

            //Si se pulsa el botón configurado...
            if (OVRInput.GetDown(buttonTeleport) == true)
                player.position = hit.point + Vector3.up;//Teletransportamos al player a esa posición +1m pa'rriba

            if (OVRInput.GetDown(buttonDoor) == true)
            {
                door.SetActive(true);
                door.transform.position = hit.point;
            }
                
            //Activamos el panel con el texto
            //panel.SetActive(true);

        }
        //Si el raycast no detecta ningún objeto
        else
        {
            //Ponemos la posición final de la línea en la misma que la inicial, para que no dibuje nada
            line.SetPosition(1, transform.position);

            //Desactivamos el panel con el texto
           // panel.SetActive(false);

        }
    }
}
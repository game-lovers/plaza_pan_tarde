﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class RayHand : MonoBehaviour
{
    LineRenderer line;
    public Animator panel;
    public Transform player;
    public OVRInput.Button teleportButton;
    TextMeshProUGUI textPanel;
    public int direction = 1;

    // Start is called before the first frame update
    void Start()
    {
        line = GetComponent<LineRenderer>();
        textPanel = GetComponentInChildren<TextMeshProUGUI>();
       // panel = GetComponentInChildren<Animator>();

    }

    // Update is called once per frame
    void Update()
    {
        
        line.SetPosition(0, transform.position);

        RaycastHit hit;

        if(Physics.Raycast(transform.position, transform.right * direction, out hit))
        {
            line.SetPosition(1, hit.point);

            //TELEPORT
            if (OVRInput.GetDown(teleportButton) == true )
                player.position = hit.point + Vector3.up;

            //INFO
            if (hit.collider.gameObject.GetComponent<InfoObject>())
            {
                textPanel.text = hit.collider.gameObject.GetComponent<InfoObject>().info;
                panel.SetBool("Open", true);
            }
            else
            {
                panel.SetBool("Open", false);
                line.SetPosition(1, transform.position);
            }
        }
        else
        {

            panel.SetBool("Open", false);
            line.SetPosition(1, transform.position);
        }
    }
}
